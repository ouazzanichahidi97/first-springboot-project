package com.getarrays.employeemanager.controller;

import com.getarrays.employeemanager.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @Test
    public void testGetAllEmployees() throws Exception{
        this.mockMvc.perform(get("/employee/all"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testGetEmployeeById() throws Exception {
        this.mockMvc.perform(get("/employee/find/{id}", 1L))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testReplaceEmployee() throws Exception {
        this.mockMvc.perform(put("/employee/update"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testCreateEmployee() throws Exception {
        this.mockMvc.perform(post("/employee/add"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        this.mockMvc.perform(delete("/employee/delete/{id}", 1L))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testDeleteAllEmployee() throws Exception {
        this.mockMvc.perform(delete("/employee/deleteAll"))
                .andDo(print()).andExpect(status().isOk());
    }
}
