package com.getarrays.employeemanager.repository;

import com.getarrays.employeemanager.model.employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepo extends CrudRepository<employee, Long> {


    Optional<employee> findEmployeeById(Long id);

}
