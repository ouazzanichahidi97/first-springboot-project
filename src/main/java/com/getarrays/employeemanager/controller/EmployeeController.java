package com.getarrays.employeemanager.controller;

import com.getarrays.employeemanager.model.employee;
import com.getarrays.employeemanager.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    EmployeeController(EmployeeService employeeService){
        this.employeeService = employeeService;
    }

    @GetMapping("/employee/all")
    public ResponseEntity<List<employee>> getAllEmployees(){
        List<employee> allEmployees = employeeService.findAll();
        return new ResponseEntity<>(allEmployees, HttpStatus.OK);
    }

    @GetMapping("/employee/find/{id}")
    public ResponseEntity<employee> getEmployeeById(@PathVariable("id") Long id){
        employee newEmployee = employeeService.findEmployeeById(id);
        return new ResponseEntity<>(newEmployee, HttpStatus.OK);
    }

    @PutMapping("/employee/update")
    public ResponseEntity<employee> replaceEmployee(@RequestBody employee employee){
        employee updatedEmployee = employeeService.updateEmployee(employee);
        return new ResponseEntity<>(updatedEmployee, HttpStatus.CREATED);
    }

    @PostMapping("/employee/add")
    public ResponseEntity<employee> createEmployee(@RequestBody employee employee){
        employee createdEmployee = employeeService.addEmployee(employee);
        return new ResponseEntity<>(createdEmployee, HttpStatus.CREATED);
    }

    @DeleteMapping("/employee/delete/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") Long id){
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/employee/deleteAll")
    public ResponseEntity<?> deleteAllEmployee(){
        List<employee> employees = employeeService.findAll();
        employeeService.deleteAllEmployee( employees);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
