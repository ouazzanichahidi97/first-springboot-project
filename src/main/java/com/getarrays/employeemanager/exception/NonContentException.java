package com.getarrays.employeemanager.exception;

import org.springframework.http.HttpStatus;

public class NonContentException extends RuntimeException {
    public NonContentException(String message) {
        super(message);
    }
}
