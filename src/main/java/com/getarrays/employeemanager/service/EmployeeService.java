package com.getarrays.employeemanager.service;


import com.getarrays.employeemanager.exception.NonContentException;
import com.getarrays.employeemanager.exception.UserNotFoundException;
import com.getarrays.employeemanager.model.employee;
import com.getarrays.employeemanager.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class EmployeeService{

    private final EmployeeRepo employeeRepo;

    EmployeeService(EmployeeRepo employeeRepo){
        this.employeeRepo = employeeRepo;
    }

    public employee addEmployee(employee employee){
        employee.setEmployeeCode(UUID.randomUUID().toString());
        return employeeRepo.save(employee);
    }

    public employee updateEmployee(employee employee){
        return employeeRepo.save(employee);
    }

    public List<employee> findAll() {
        List<employee> employees = (List<employee>) employeeRepo.findAll();
        if (!employees.isEmpty())
            return employees;
        else {
            throw new NonContentException("employees list is empty");
        }
    }

    public employee findEmployeeById(Long id){
        return  employeeRepo.findEmployeeById(id)
                .orElseThrow(() -> new UserNotFoundException("user " + id +" not found"));
    }

    public void deleteEmployee(long id) {
        employeeRepo.deleteById(id);
    }

    public void deleteAllEmployee(List<employee> employees) {
        employeeRepo.deleteAll(employees);
    }

}

